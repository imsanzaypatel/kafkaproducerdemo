package com.kafka;
public class SimpleKafkaTest {

  @Rule
  public EmbeddedKafkaCluster cluster = provisionWith(useDefaults());

  @Test
  public void shouldWaitForRecordsToBePublished() throws Exception {
    cluster.send(to("test-topic", "a", "b", "c").useDefaults());
    cluster.observe(on("test-topic", 4).useDefaults());
  }
}