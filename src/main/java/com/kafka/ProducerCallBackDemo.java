package com.kafka;

import java.util.Properties;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ProducerCallBackDemo 
{
    public static void main( String[] args ){
    	
    	final Logger logger=LoggerFactory.getLogger(ProducerCallBackDemo.class);
    	
    	String bootstrapServers="127.0.0.1:9092";

    	//create Producer properties
    	Properties properties = new Properties();
    	properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
    	properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    	properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    	
    	//create kafka producer
    	KafkaProducer<String,String> kafkaProducer= new KafkaProducer<String, String>(properties);
    	
    	//create producer record
    	ProducerRecord<String,String> record=
    			new ProducerRecord<String,String>("first_topic","hello Kafka world");
    	//send data asynchronous
    	kafkaProducer.send(record,new Callback() {
			
			public void onCompletion(RecordMetadata recordMetadata, Exception exception) {
				//execute every time a record is succesfully sent or an exception is thrown 
			if(exception !=null) {
				//record is succesfully sent 
				logger.info("Reciveed new metadata. \n" +"Topic: "+recordMetadata.topic() +"\n"+
				"Partition:"+recordMetadata.partition()+"\n"+
				 "Offset" +recordMetadata.offset()+"\n"+
				 "TimeStamp"+recordMetadata.timestamp());
				
			}
			else {
				logger.error("Error while producing msg", exception);
			}
			}
		});
    	
    	kafkaProducer.flush();
    	
    	kafkaProducer.close();
    }
}
