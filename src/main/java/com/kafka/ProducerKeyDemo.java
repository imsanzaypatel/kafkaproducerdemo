package com.kafka;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ProducerKeyDemo 
{
    public static void main( String[] args ){
    	
    	final Logger logger=LoggerFactory.getLogger(ProducerKeyDemo.class);
    	
    	String bootstrapServers="127.0.0.1:9092";

    	//create Producer properties
    	Properties properties = new Properties();
    	properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
    	properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    	properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    	
    	//create kafka producer
    	KafkaProducer<String,String> kafkaProducer= new KafkaProducer<String, String>(properties);
    	
    	for(int i=0;i<10;i++) {
    		
    	String topic ="first_topic";
    	String value="hello Kafka world" + Integer.toString(i);
    	String key="Id_" + Integer.toString(i);
    	//create producer record
    	ProducerRecord<String,String> record=
    			new ProducerRecord<String,String>(topic,key,value);
    	
    	logger.info("Key:",key);
    	
    	//send data asynchronous
    	try {
			kafkaProducer.send(record,new Callback() {
				
				public void onCompletion(RecordMetadata recordMetadata, Exception exception) {
					//execute every time a record is succesfully sent or an exception is thrown 
				if(exception !=null) {
					//record is succesfully sent 
					logger.info("Reciveed new metadata. \n" +"Topic: "+recordMetadata.topic() +"\n"+
					"Partition:"+recordMetadata.partition()+"\n"+
					 "Offset" +recordMetadata.offset()+"\n"+
					 "TimeStamp"+recordMetadata.timestamp());
					
				}
				else {
					logger.error("Error while producing msg", exception);
				}
				}
			}).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
    	}
    	kafkaProducer.flush();
    	
    	kafkaProducer.close();
    }
}
